package aggregation;

import java.io.IOException;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Calendar;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import connection.MeasuresDAO;
import connection.MeasuresInput;

public class SmartDu extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private MeasuresDAO measuresDAO = new MeasuresDAO();

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String time = request.getParameter("time");
		String deviceIDs = request.getParameter("deviceIDs");
		String appGardenID = request.getParameter("appGardenID");
 		String measures = request.getParameter("measures");

		Calendar cal = Calendar.getInstance();

		java.util.Date nowCal = cal.getTime();
		java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(nowCal.getTime());

		switch (time) {

		case "Hour":
			cal.add(Calendar.HOUR_OF_DAY, -1);
			break;

		case "Day":
			cal.add(Calendar.DATE, -1);
			break;

		case "Week":
			cal.add(Calendar.WEEK_OF_MONTH, -1);
			break;

		case "Month":
			cal.add(Calendar.MONTH, -1);
			break;

		case "Year":
			cal.add(Calendar.YEAR, -1);
			break;

		default:
			break;
		}

		java.util.Date intervalCal = cal.getTime();
		Timestamp intervalTimestamp = new java.sql.Timestamp(intervalCal.getTime());

		long now = currentTimestamp.getTime();
		long interval = intervalTimestamp.getTime();
		

		MeasuresInput measuresInput = new MeasuresInput(appGardenID, deviceIDs, now, interval, measures);

		String measursGson = null;

		try {
			measursGson = measuresDAO.getMeasures(measuresInput);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(measursGson);

	}

}
