package status;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Calendar;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import connection.*;

/**
 * Servlet implementation class DeviceStatus
 */
public class DeviceStatus extends HttpServlet {

	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public DeviceStatus() {
		super();
		// TODO Auto-generated constructor stub
	}

	private MeasuresDAO measuresDAO = new MeasuresDAO();

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String deviceID = request.getParameter("deviceUid");
		String appGardenID = request.getParameter("appGardenID");
		String measures = request.getParameter("measures");

		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MINUTE, -40);
		java.util.Date absCal = cal.getTime();
		java.sql.Timestamp absTimestamp = new java.sql.Timestamp(absCal.getTime());

		MeasuresInput measuresInput = new MeasuresInput();

		measuresInput.setAppGardenID(appGardenID);
		measuresInput.setDeviceIDs(deviceID);
		measuresInput.setMeasures(measures);
		measuresInput.setStartTime(absTimestamp.getTime());

		String measursGson = null;

		try {
			measursGson = measuresDAO.getDeviceStatus(measuresInput);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(measursGson);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
