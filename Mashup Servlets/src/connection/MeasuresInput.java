package connection;

public class MeasuresInput {
	    
	    
		public MeasuresInput() {
			super();
			// TODO Auto-generated constructor stub
		}
		
		 private String appGardenID;
		    private String deviceIDs;
		    private long startTime;
		    private long endTime;
		    private String measures;
		public MeasuresInput(String appGardenID, String deviceIDs, long startTime, long endTime, String measures) {
			super();
			this.appGardenID = appGardenID;
			this.deviceIDs = deviceIDs;
			this.startTime = startTime;
			this.endTime = endTime;
			this.measures = measures;
		}
		public String getAppGardenID() {
			return appGardenID;
		}
		public void setAppGardenID(String appGardenID) {
			this.appGardenID = appGardenID;
		}
		public String getDeviceIDs() {
			return deviceIDs;
		}
		public void setDeviceIDs(String deviceIDs) {
			this.deviceIDs = deviceIDs;
		}
		public long getStartTime() {
			return startTime;
		}
		public void setStartTime(long startTime) {
			this.startTime = startTime;
		}
		public long getEndTime() {
			return endTime;
		}
		public void setEndTime(long endTime) {
			this.endTime = endTime;
		}
		public String getMeasures() {
			return measures;
		}
		public void setMeasures(String measures) {
			this.measures = measures;
		}
	    

}
