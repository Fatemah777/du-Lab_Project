package connection;

import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import com.mysql.jdbc.Driver;

import java.sql.Connection;
import java.sql.ResultSet;

public class ConnectionFactory {
	private static Properties DB_PROPERTIES;
	private static Driver DRIVER;
	private static String URL;

	static{
		
		DB_PROPERTIES = new Properties();
		InputStream resourceAsStream =null;
	 	try {
			
	 		  resourceAsStream = ConnectionFactory.class.getClassLoader().getResourceAsStream("/properties/conn.properties");
	 		 DB_PROPERTIES.load(resourceAsStream);
		 
			DRIVER = (Driver) Class.forName(DB_PROPERTIES.getProperty("jdbc.driver")).newInstance();
			URL = DB_PROPERTIES.getProperty("db.url");
			 
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			try {
				resourceAsStream.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	
		
	}

	public static Connection getConnection() throws SQLException {
		return DRIVER.connect(URL, DB_PROPERTIES);
	}

	public static void closeConnection(Connection conn, Statement stmt, ResultSet rs) throws SQLException {
		if (conn != null) {
			conn.close();
		}

		if (stmt != null) {
			stmt.close();
		}

		if (rs != null) {
			rs.close();
		}
	}

}
