package connection;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;


import com.google.gson.Gson;

public class MeasuresDAO {

	Connection connection = null;

	public String getLatestValue(String label, Long maxTime)
			throws SQLException {
		Statement stmt = null;
		ResultSet rs = null;
		String result = "0.0";

		try {
			connection = ConnectionFactory.getConnection();

			stmt = connection.createStatement();

			String sql = "SELECT p.payloadstr FROM PacketDb.Packet p inner join PacketDb.Configuration c "
					+ "on (p.configid=c.idconfig) WHERE  c.attrlabel = '" + label + "' AND  p.starttime = " + maxTime;

			rs = stmt.executeQuery(sql);

			if (rs.next()) {
				Double res = round(rs.getDouble(1),2);
				result = res.toString();
			} else if (label == "WindDirection") {
				result = "N";
			}

		} finally {
			ConnectionFactory.closeConnection(connection, stmt, rs);
		}

		return result;
	}

	public String getMeasures(MeasuresInput measures) throws SQLException {
		
		Statement stmt = null;
		ResultSet rs = null;
		
		StringBuilder sb = new StringBuilder("{ ");
		String prefix = "";

		String label = null, latest = "0.0";
		double sumValue = 0.0, maxValue = 0.0, minValue = 0.0, avgValue = 0.0;
		Long maxTime = 0L;
		java.util.Date maxTimeStamp;
		
		String gson = null;

		try {
			connection = ConnectionFactory.getConnection();

			stmt = connection.createStatement();
			String sql = "SELECT c.attrlabel, SUM(p.payloadstr), MAX(p.payloadstr), MIN(p.payloadstr), AVG(p.payloadstr), MAX(p.starttime) "
					+ " FROM PacketDb.Packet p inner join PacketDb.Configuration c on(p.configid=c.idconfig) "
					+ " inner join PacketDb.LogicalNode ln on(c.logicalnodeid=ln.id) inner join PacketDb.PhysicalNode pn on (ln.physicalnodeid = pn.id) "
					+ " Where pn.id IN(" + measures.getDeviceIDs() + ") AND c.attrlabel IN (" + measures.getMeasures()
					+ " )" + " AND p.starttime BETWEEN " + measures.getEndTime() + " AND " + measures.getStartTime()
					+ " AND p.appgardenid =" + measures.getAppGardenID() + " group by c.attrlabel";

			rs = stmt.executeQuery(sql);

			while (rs.next()) {

				label = rs.getString("attrlabel");
				sumValue = round(rs.getDouble("SUM(p.payloadstr)"),2);
				maxValue = round(rs.getDouble("MAX(p.payloadstr)"),2);
				avgValue = round(rs.getDouble("AVG(p.payloadstr)"),2);
				minValue = round(rs.getDouble("MIN(p.payloadstr)"),2);
				maxTime = rs.getLong("MAX(p.starttime)");
				

				maxTimeStamp = new java.util.Date(maxTime);

				latest = getLatestValue(label, maxTime);


				label = label.replaceAll("\\s", "");

				sb.append(prefix);
				prefix = ",";
				
				sb.append(" " + label + "SumValue :" + sumValue + " , " + label + "MaxValue :" + maxValue
						+ ", " + label + "AvgValue :" + avgValue + " , " + label + "MinValue :" + minValue
						+ ", " + label + "LatestValue :" + latest + " , " + label + "MaxTime : "
						+ maxTimeStamp.toLocaleString() + " ");
			}

			sb.append(" }");
			
			System.out.println("//////////////%%%%%%%%%%%%%%%"+sb);
			
			 gson = new Gson().toJson(sb.toString());
			

		} finally {
			ConnectionFactory.closeConnection(connection, stmt, rs);
		}

		return gson;
	}
	
	public double round(double value, int places) {
	    if (places < 0) throw new IllegalArgumentException();

	    BigDecimal bd = new BigDecimal(value);
	    bd = bd.setScale(places, RoundingMode.HALF_UP);
	    return bd.doubleValue();
	}
	
	public String  getDeviceStatus(MeasuresInput measuresInput) throws SQLException {

		Statement stmt = null;
		ResultSet rs = null;
		
		String status="off";
		String gson = null;
		
		Long maxTime = 0L;
		int counter=0;
		
		java.util.Date lastMeasureTime, beforeFortyMinuts;

		try {
			connection = ConnectionFactory.getConnection();

			stmt = connection.createStatement();
			String sql = "SELECT c.attrlabel, MAX(p.starttime) "
					+ " FROM PacketDb.Packet p inner join PacketDb.Configuration c on(p.configid=c.idconfig) "
					+ " inner join PacketDb.LogicalNode ln on(c.logicalnodeid=ln.id) inner join PacketDb.PhysicalNode pn on (ln.physicalnodeid = pn.id) "
					+ " Where pn.id IN(" + measuresInput.getDeviceIDs() + ") AND c.attrlabel IN ( " + measuresInput.getMeasures() + " )"
					+ " AND p.appgardenid =" + measuresInput.getAppGardenID()
					+ " group by c.attrlabel";

		   rs = stmt.executeQuery(sql);
			

			while (rs.next()) {

				maxTime = rs.getLong("MAX(p.starttime)");

				lastMeasureTime = new Date(maxTime);
				beforeFortyMinuts = new Date(measuresInput.getStartTime());

				 if(lastMeasureTime.after(beforeFortyMinuts)){
					 counter++;
				 }

			}

			String[] attArr = measuresInput.getMeasures().split("\\,");
            if(counter==attArr.length){
            	status="on";
            }else if(counter>0){
            	status="warn";
            }
            
            gson = new Gson().toJson(" { status :"+status+" }");
			

		} finally {
			ConnectionFactory.closeConnection(connection, stmt, rs);
		}

		return gson;
	}

}
